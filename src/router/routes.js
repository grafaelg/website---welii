
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'soluciones', component: () => import('pages/solucion-2.vue') },
      { path: 'solucion-1', component: () => import('pages/solucion-1.vue') },
      { path: 'solucion-3', component: () => import('pages/solucion-3.vue') },
      { path: 'gowelii', component: () => import('pages/gowelii.vue') },
      { path: 'gomedisys', component: () => import('pages/gomedisys.vue') },
      { path: 'contacto', component: () => import('pages/contacto.vue') },
      { path: 'servicios-gowelii', component: () => import('pages/servicios-gowelii.vue') },
      { path: 'servicios-gomedisys', component: () => import('pages/servicios-gomedisys.vue') }
    ]
  }
]
// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
